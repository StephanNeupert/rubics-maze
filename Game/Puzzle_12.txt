% Cube 12
START 0, 0, 0
 
WALL  0, 0, 0, Back, Rotatable
WALL  0, 0, 0, Down, Rotatable
WALL  0, 0, 0, Left, Rotatable
 
WALL  0, 2, 0, Front
WALL  1, 2, 0, Front
WALL  0, 1, 0, Front
WALL  1, 1, 0, Front
WALL  2, 1, 0, Front 
WALL  1, 0, 0, Front
WALL  1, 1, 1, Front
WALL  1, 0, 1, Front

WALL  1, 1, 1, Back
WALL  1, 0, 1, Back
WALL  1, 2, 2, Back
WALL  2, 2, 2, Back
WALL  1, 1, 2, Back
WALL  2, 1, 2, Back
WALL  1, 0, 2, Back

WALL  1, 0, 0, Up
WALL  2, 0, 0, Up
WALL  1, 1, 1, Up
WALL  0, 0, 1, Up
WALL  2, 0, 1, Up
WALL  1, 0, 2, Up
WALL  2, 0, 2, Up

WALL  1, 2, 0, Down
WALL  2, 2, 0, Down
WALL  0, 2, 1, Down
WALL  1, 2, 1, Down
WALL  2, 2, 1, Down
WALL  1, 2, 2, Down
WALL  0, 1, 2, Down

WALL  0, 1, 0, Right
WALL  0, 2, 1, Right
WALL  0, 1, 1, Right
WALL  1, 1, 1, Right
WALL  0, 0, 1, Right
WALL  0, 2, 2, Right
WALL  0, 1, 2, Right
WALL  0, 0, 2, Right

WALL  2, 1, 0, Left
WALL  2, 2, 1, Left
WALL  1, 1, 1, Left
WALL  2, 1, 1, Left
WALL  1, 0, 1, Left
WALL  2, 0, 1, Left
WALL  2, 1, 2, Left
 