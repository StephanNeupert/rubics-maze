-------------------
|                 |
|  Rubic's Maze   |
|                 |
-------------------

1. Goal
Move through the maze within a 3x3x3 cube to reach the center.
But to achieve this, you will have to modify the maze by 
rotating its faces.

2. Walls
To make navigating the maze easier, the walls are colored differently:
- Gray walls: Usual walls - the darker the farther away.
- Red walls: Standing in front of these walls allows you to rotate the 
             face of the cube containing it.
- Blue walls: Usual walls, but seen from behind.

3. Navigation
The navigation is done via keys:
- Arrow Up:    Move forward
- Arrow Down:  Move backward
- Arrow Right: Turn right
- Arrow Left:  Turn left
- Key "I":     Move forward
- Key "N":     Move backward
- Key "K":     Move right (without turning)
- Key "H":     Move left (without turning)
- Key "U":     Move up (without turning)
- Key "M":     Move down (without turning)
- Key "D":     Turn right
- Key "A":     Turn left
- Key "E":     Flip right (without moving), so that the eyes keep looking forward, but the feet are now facing left
- Key "Y":     Flip left (without moving), so that the eyes keep looking forward, but the feet are now facing right
- Key "W":     Fall forward (without turning), so that the eyes now look down with the feet pointing backwards
- Key "X":     Fall backward (without turning), so that the eyes now look up with the feet pointing forward
- Space:       Rotate face of cube clock-wise (only when facing a red wall)
- Key "J":     Rotate face of cube clock-wise (only when facing a red wall)
- Key "S":     Rotate face of cube counter-clock-wise (only when facing a red wall)

4. Other keys
- Return:      Start next level
- Backspace:   Start previous level
- Home Key:    Restart current level
- Escape:      Quits the game

5. Legal stuff
Copyright: All rights reserved (regarding game idea, levels and code) by Stephan Neupert, 2017