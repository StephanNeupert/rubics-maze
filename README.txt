Rubic's Maze

by Stephan Neupert

This is a small maze game within a Rubic's Cube, mainly made to get used to WPF.
Standing within the cube, you can rotate its sides at selected positions
to create a path to the center of the cube.

Compile informations:
- Build "Rubic's Maze.sln" (e.g. with Visual Studio 2015 or newer)
- The editor requires C# 6.0 (or newer)
- There are no dependencies on external libraries

To build on Linux, install Mono and run xbuild without any arguments. 

Copyright: CC BY-NC 4.0 (2017)