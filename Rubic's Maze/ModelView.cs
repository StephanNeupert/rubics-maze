﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.IO;
using System.Windows.Input;
using System.ComponentModel;


namespace Rubic_Maze
{
    class ModelView : INotifyPropertyChanged
    {
        public ModelView()
        {
            WallShapes = new ObservableCollection<WallShape>();
            Title = "";

            CreatePuzzleNameList();
            curLevelIndex = 0;
            LoadNewLevel(0);
        }

        public ICommand QuitCommand => new ActionCommand(() => Application.Current.Shutdown());

        public ICommand NextLevelCommand => new ActionCommand(() => LoadNewLevel(1));
        public ICommand PreviousLevelCommand => new ActionCommand(() => LoadNewLevel(-1));
        public ICommand RestartLevelCommand => new ActionCommand(() => RestartLevel());

        public ICommand RotatePlayerCommand => new DelegateCommand((p) => RotatePlayer((C.Dir)p));
        public ICommand MovePlayerCommand => new DelegateCommand((p) => MovePlayer((C.Dir)p));
        public ICommand RotateWallCommand => new DelegateCommand((p) => RotateWall((C.Dir)p));

        private ObservableCollection<WallShape> wallShapes;
        private string title;

        List<string> levelPaths;
        int curLevelIndex;

        Level startLevel;
        Level curLevel;
        Renderer renderer;

        public int CanvasHeight => C.CanvasHeight;
        public int CanvasWidth => C.CanvasWidth;
        public int BorderHeight_1 => C.CanvasHeight + 2;
        public int BorderWidth_1 => C.CanvasWidth + 2;
        public int BorderHeight_2 => C.CanvasHeight + 6;
        public int BorderWidth_2 => C.CanvasWidth + 6;

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises the property changed event.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        private void RaisePropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Load the sorted list of all levels.
        /// </summary>
        private void CreatePuzzleNameList()
        {
            levelPaths = Directory.GetFiles(C.AppPath, "*.txt", SearchOption.TopDirectoryOnly)
                                   .Select(file => Path.GetFileName(file))
                                   .ToList()
                                   .FindAll(file => file.StartsWith("Puzzle"));
            levelPaths.Sort();
        }

        /// <summary>
        /// Selects a new level and loads it.
        /// </summary>
        /// <param name="levelDelta"></param>
        public void LoadNewLevel(int levelDelta)
        {
            if (levelPaths.Count == 0)
            {
                // Load default level and disable buttons.
                startLevel = new Level();
            }
            else
            {
                curLevelIndex = (curLevelIndex + levelPaths.Count + (levelDelta % levelPaths.Count)) % levelPaths.Count;
                startLevel = new Level(levelPaths[curLevelIndex]);
            }

            curLevel = startLevel.Clone();
            renderer = new Renderer(curLevel);

            WallShapes = renderer.CreateImage();
            UpdateTitle();
        }

        /// <summary>
        /// Restarts the current level
        /// </summary>
        public void RestartLevel()
        {
            curLevel = startLevel.Clone();
            renderer = new Renderer(curLevel);

            WallShapes = renderer.CreateImage();
            UpdateTitle();
        }

        /// <summary>
        /// Moves the player and returns whether it was possible.
        /// </summary>
        /// <param name="direction"></param>
        /// <returns></returns>
        public bool MovePlayer(C.Dir direction)
        {
            if (curLevel.IsSolved()) return false;

            bool hasMoved = curLevel.TryMovePlayer(direction);
            if (hasMoved) WallShapes = renderer.CreateImage();

            UpdateTitle(); // See whether the level is solved or not.

            return hasMoved;
        }

        /// <summary>
        /// Rotates the player.
        /// </summary>
        /// <param name="direction"></param>
        /// <returns></returns>
        public bool RotatePlayer(C.Dir direction)
        {
            if (curLevel.IsSolved()) return false;

            curLevel.RotatePlayer(direction);
            WallShapes = renderer.CreateImage();

            return true;
        }

        /// <summary>
        /// Rotates the wall in front of the player and returns whether it was possible.
        /// </summary>
        /// <param name="direction"></param>
        /// <returns></returns>
        public bool RotateWall(C.Dir direction)
        {
            if (curLevel.IsSolved()) return false;

            bool hasMoved = curLevel.TryRotateWall(direction);
            if (hasMoved) WallShapes = renderer.CreateImage();

            return hasMoved;
        }

        /// <summary>
        /// The title text
        /// </summary>
        public string Title
        {
            get { return title; }
            set
            {
                if (!value.Equals(title))
                {
                    title = value;
                    RaisePropertyChanged("Title");
                } 
            }
        }

        /// <summary>
        /// The schapes of the walls.
        /// </summary>
        public ObservableCollection<WallShape> WallShapes
        {
            get { return wallShapes; }
            set { wallShapes = value; RaisePropertyChanged("WallShapes"); }
        }


        /// <summary>
        /// Generates the proper title name.
        /// </summary>
        private void UpdateTitle()
        {
            Title = "Rubic's Maze - Level " + (curLevelIndex + 1).ToString()
                  + (curLevel.IsSolved() ? " - Solved!" : string.Empty);
        }


    }
}
