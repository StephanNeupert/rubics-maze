﻿using System;
using System.Windows.Media;

namespace Rubic_Maze
{
    class WallShape
    {
        public WallShape(double top, double left, PointCollection points, Brush stroke, int thickness, Brush fill)
        {
            CanvasTop = top;
            CanvasLeft = left;
            Points = points;
            Stroke = stroke;
            StrokeThickness = thickness;
            Fill = fill;
        }

        public double CanvasTop { get; private set; }
        public double CanvasLeft { get; private set; }
        public PointCollection Points { get; private set; }
        public Brush Stroke { get; private set; }
        public int StrokeThickness { get; private set; }
        public Brush Fill { get; private set; }
    }

    class WallShapeFactory
    {
        public WallShapeFactory() { }

        public double CanvasTop;
        public double CanvasLeft;
        public PointCollection Points;
        public Brush Stroke;
        public int StrokeThickness;
        public Brush Fill;

        public WallShape Create()
        {
            return new WallShape(CanvasTop, CanvasLeft, Points, Stroke, StrokeThickness, Fill);
        }
    }
}
