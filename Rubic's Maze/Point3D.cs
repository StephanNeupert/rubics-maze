﻿using System;

namespace Rubic_Maze
{
    class Point3D : Orientable<Point3D>, IEquatable<Point3D>
    {
        public Point3D(int posX, int posY, int posZ)
        {
            X = posX;
            Y = posY;
            Z = posZ;
        }

        public Point3D(Point3D point)
        {
            X = point.X;
            Y = point.Y;
            Z = point.Z;
        }

        public int X { get; private set; }
        public int Y { get; private set; }
        public int Z { get; private set; }

        public bool Equals(Point3D point)
        {
            return X == point.X && Y == point.Y && Z == point.Z;
        }

        public Point3D Move(C.Dir dir, int length = 1)
        {
            switch (dir)
            {
                case C.Dir.Front: return new Point3D(X, Y, Z + length);
                case C.Dir.Back: return new Point3D(X, Y, Z - length);
                case C.Dir.Right: return new Point3D(X + length, Y, Z);
                case C.Dir.Left: return new Point3D(X - length, Y, Z);
                case C.Dir.Top: return new Point3D(X, Y + length, Z);
                case C.Dir.Bottom: return new Point3D(X, Y - length, Z);
                default: return this;
            }
        }

        /// <summary>
        /// Turns around the y-axis to the right.
        /// </summary>
        /// <returns></returns>
        public override Point3D TurnRight()
        {
            return new Point3D(Z, Y, -X);
        }

        /// <summary>
        /// Turns around the y-axis to the left.
        /// </summary>
        /// <returns></returns>
        public override Point3D TurnLeft()
        {
            return new Point3D(-Z, Y, X);
        }

        /// <summary>
        /// Turns around the x-axis to the top.
        /// </summary>
        /// <returns></returns>
        public override Point3D FlipBack()
        {
            return new Point3D(X, Z, -Y);
        }

        /// <summary>
        /// Rotates a point to the right of left in the square of size C.Size
        /// </summary>
        /// <returns></returns>
        public override Point3D Rotate(C.Dir dir)
        {
            switch (dir)
            {
                case C.Dir.Right: return new Point3D(Y, -X, Z);
                case C.Dir.Left: return new Point3D(-Y, X, Z);
                default: return this;
            }
        }

        /// <summary>
        /// Rotates the point in absolute coordinates in front of the player to the left.
        /// </summary>
        /// <param name="playerEyes"></param>
        /// <returns></returns>
        public override Point3D RotateFromPlayer(C.Dir playerFeet, C.Dir playerEyes, Point3D playerPos, C.Dir rotateDir)
        {
            Point3D faceCenter; // the center of the rotating face
            switch (playerEyes)
            {
                case C.Dir.Front:
                case C.Dir.Back:
                    faceCenter = new Point3D(1, 1, playerPos.Z); break;
                case C.Dir.Top:
                case C.Dir.Bottom:
                    faceCenter = new Point3D(1, playerPos.Y, 1); break;
                case C.Dir.Right:
                case C.Dir.Left:
                    faceCenter = new Point3D(playerPos.X, 1, 1); break;
                default: faceCenter = new Point3D(1, 1, 1); break;
            }

            // Compute the coordinates relative to the faceCenter
            Point3D relPoint = new Point3D(X - faceCenter.X, Y - faceCenter.Y, Z - faceCenter.Z);
            // Rotate the point there
            Point3D rotRelPoint = relPoint.OrientToAbsolute(C.Dir.Bottom, playerEyes)
                                          .Rotate(rotateDir)
                                          .OrientToPlayer(C.Dir.Bottom, playerEyes);
            // Translate it back to absolute coordinates
            return new Point3D(rotRelPoint.X + faceCenter.X, rotRelPoint.Y + faceCenter.Y, rotRelPoint.Z + faceCenter.Z);
        }
    }
}
