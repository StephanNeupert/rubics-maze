﻿using System;

namespace Rubic_Maze
{
    /// <summary>
    /// Some constants
    /// </summary>
    public static class C
    {
        public static char DirSep => System.IO.Path.DirectorySeparatorChar;
        public static string NewLine => Environment.NewLine;

        public static string AppPath => AppDomain.CurrentDomain.BaseDirectory + DirSep;

        public static readonly int Size = 3;
        public static int MidCoord => (Size - 1) / 2;

        public static readonly double ShrinkFactor = 0.6;

        public enum Dir { Right, Left, Top, Bottom, Front, Back };

        public static readonly int CanvasWidth = 320;
        public static readonly int CanvasHeight = 320;
    }
}
