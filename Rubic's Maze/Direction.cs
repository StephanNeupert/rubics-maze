﻿using System;

namespace Rubic_Maze
{
    /// <summary>
    /// Handles modifications of directions.
    /// </summary>
    class Direction : Orientable<Direction>, IEquatable<Direction>, IComparable<Direction>
    {
        public Direction(C.Dir dir)
        {
            Dir = dir;
        }

        public C.Dir Dir;

        public bool Equals(Direction direction)
        {
            return Dir == direction.Dir;
        }

        public int CompareTo(Direction direction)
        {
            if (this.Dir == direction.Dir) return 0;
            else if (this.Dir == C.Dir.Front) return -1;
            else if (this.Dir == C.Dir.Back) return 1;
            else if (direction.Dir == C.Dir.Front) return 1;
            else if (direction.Dir == C.Dir.Back) return -1;
            else return 0;
        }

        public Direction Opposite()
        {
            switch (Dir)
            {
                case C.Dir.Front: return new Direction(C.Dir.Back);
                case C.Dir.Right: return new Direction(C.Dir.Left);
                case C.Dir.Left: return new Direction(C.Dir.Right);
                case C.Dir.Back: return new Direction(C.Dir.Front);
                case C.Dir.Top: return new Direction(C.Dir.Bottom);
                case C.Dir.Bottom: return new Direction(C.Dir.Top);
                default: return this;
            }
        }

        /// <summary>
        /// Turns around the y-axis to the right.
        /// </summary>
        /// <param name="Dir"></param>
        /// <returns></returns>
        public override Direction TurnRight()
        {
            switch (Dir)
            {
                case C.Dir.Front: return new Direction(C.Dir.Right);
                case C.Dir.Right: return new Direction(C.Dir.Back);
                case C.Dir.Left: return new Direction(C.Dir.Front);
                case C.Dir.Back: return new Direction(C.Dir.Left);
                default: return this;
            }
        }

        /// <summary>
        /// Turns around the y-axis to the left.
        /// </summary>
        /// <returns></returns>
        public override Direction TurnLeft()
        {
            switch (Dir)
            {
                case C.Dir.Front: return new Direction(C.Dir.Left);
                case C.Dir.Right: return new Direction(C.Dir.Front);
                case C.Dir.Left: return new Direction(C.Dir.Back);
                case C.Dir.Back: return new Direction(C.Dir.Right);
                default: return this;
            }
        }

        /// <summary>
        /// Turns around the x-axis to the top.
        /// </summary>
        /// <returns></returns>
        public override Direction FlipBack()
        {
            switch (Dir)
            {
                case C.Dir.Front: return new Direction(C.Dir.Top);
                case C.Dir.Top: return new Direction(C.Dir.Back);
                case C.Dir.Bottom: return new Direction(C.Dir.Front);
                case C.Dir.Back: return new Direction(C.Dir.Bottom);
                default: return this;
            }
        }

        /// <summary>
        /// Rotates the direction to the right. 
        /// </summary>
        /// <returns></returns>
        public override Direction Rotate(C.Dir rotateDir)
        {
            switch (rotateDir)
            {
                case C.Dir.Right:
                    {
                        switch (Dir)
                        {
                            case C.Dir.Top: return new Direction(C.Dir.Right);
                            case C.Dir.Bottom: return new Direction(C.Dir.Left);
                            case C.Dir.Left: return new Direction(C.Dir.Top);
                            case C.Dir.Right: return new Direction(C.Dir.Bottom);
                            default: return this;
                        }
                    }
                case C.Dir.Left:
                    {
                        switch (Dir)
                        {
                            case C.Dir.Top: return new Direction(C.Dir.Left);
                            case C.Dir.Bottom: return new Direction(C.Dir.Right);
                            case C.Dir.Left: return new Direction(C.Dir.Bottom);
                            case C.Dir.Right: return new Direction(C.Dir.Top);
                            default: return this;
                        }
                    }
                default: return this;
            }
            
        }

        /// <summary>
        /// Rotates the direction in absolute coordinates in front of the player to the left.
        /// </summary>
        /// <param name="playerEyes"></param>
        /// <returns></returns>
        public override Direction RotateFromPlayer(C.Dir playerFeet, C.Dir playerEyes, Point3D playerPos, C.Dir rotateDir)
        {
            return OrientToAbsolute(playerFeet, playerEyes)
                    .Rotate(rotateDir)
                    .OrientToPlayer(playerFeet, playerEyes);
        }

    }
}
