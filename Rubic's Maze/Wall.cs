﻿using System;

namespace Rubic_Maze
{
    /// <summary>
    /// A wall of a level.
    /// </summary>
    class Wall : IEquatable<Wall>, IComparable<Wall>
    {
        public Wall(Point3D pos, Direction dir, bool isRotatable = false)
        {
            Pos = pos;
            Dir = dir;
            IsRotatable = isRotatable;
        }

        public Wall(Point3D pos, C.Dir dir, bool isRotatable = false)
        {
            Pos = pos;
            Dir = new Direction(dir);
            IsRotatable = isRotatable;
        }

        public Point3D Pos { get; set; }
        public Direction Dir { get; set; }
        public bool IsRotatable { get; private set; }

        public Wall Clone()
        {
            return new Wall(new Point3D(Pos), Dir, IsRotatable);
        }

        /// <summary>
        /// Checks whether the walls have the same position. 
        /// <para> Returns true even when one is rotatable and the other is not. </para>
        /// </summary>
        /// <param name="wall"></param>
        /// <returns></returns>
        public bool Equals(Wall wall)
        {
            return this.Pos.Equals(wall.Pos) && this.Dir.Equals(wall.Dir);
        }

        public int CompareTo(Wall wall)
        {
            // First sort wrt. z-coordinate (but in descending order)
            if (Pos.Z != wall.Pos.Z) return wall.Pos.Z.CompareTo(Pos.Z);
            // Then sort back and front walls.
            if (Dir.CompareTo(wall.Dir) != 0) return Dir.CompareTo(wall.Dir);
            // then sort wrt. Manhatten-distance in x-y-plane (but in descending order)
            if (Distance() != wall.Distance()) return wall.Distance().CompareTo(Distance());
            // Then sort according to disance to the origin, while taking the direction into accound
            bool isThisOuter = (Dir.Dir == C.Dir.Top && Pos.Y >= 0)
                            || (Dir.Dir == C.Dir.Bottom && Pos.Y <= 0)
                            || (Dir.Dir == C.Dir.Right && Pos.X >= 0)
                            || (Dir.Dir == C.Dir.Left && Pos.X <= 0);
            bool isWallOuter = (wall.Dir.Dir == C.Dir.Top && wall.Pos.Y >= 0)
                            || (wall.Dir.Dir == C.Dir.Bottom && wall.Pos.Y <= 0)
                            || (wall.Dir.Dir == C.Dir.Right && wall.Pos.X >= 0)
                            || (wall.Dir.Dir == C.Dir.Left && wall.Pos.X <= 0);
            if (isThisOuter == isWallOuter) return 0;
            else if (isThisOuter == true) return -1;
            else /*if (isWallOuter == true)*/ return 1;
        }

        /// <summary>
        /// Determines whether a given wall is visible.
        /// </summary>
        /// <returns></returns>
        public bool IsVisible()
        {
            if (Pos.Z > 0) return true;
            else if (Pos.Z == 0) return !(Dir.Dir == C.Dir.Back);
            else return false;
        }

        /// <summary>
        /// Distance from the origin.
        /// </summary>
        /// <returns></returns>
        private int Distance()
        {
            return Math.Abs(Pos.X) + Math.Abs(Pos.Y);
        }

        /// <summary>
        /// Whether one sees it from the back, standing at (0, 0, 0)
        /// </summary>
        /// <returns></returns>
        public bool IsSeenFromBack()
        {
            switch (Dir.Dir)
            {
                case C.Dir.Back: return true;
                case C.Dir.Bottom: return (Pos.Y > 0);
                case C.Dir.Top: return (Pos.Y < 0);
                case C.Dir.Right: return (Pos.X < 0);
                case C.Dir.Left: return (Pos.X > 0);
                default: return false;
            }
        }


    }
}
