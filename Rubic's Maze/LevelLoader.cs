﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace Rubic_Maze
{
    class LevelLoader
    {
        public LevelLoader() { }

        /// <summary>
        /// Loads a level from a given text file.
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public Level Load(string filePath, Level level)
        {
            if (!File.Exists(filePath)) return level;

            try
            {
                var fileLines = ReadFile(filePath);
                fileLines.ForEach(line => EvaluateLine(level, line));
            }
            catch
            {
                // can't do much here
            }

            return level;
        }

        /// <summary>
        /// Reads in all files of the level.
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        private static List<string> ReadFile(string filePath)
        {
            List<string> fileLines = new List<string>();

            try
            {
                // Open the text file using a stream reader.
                using (StreamReader fileReader = new StreamReader(filePath))
                {
                    string line;
                    while ((line = fileReader.ReadLine()) != null)
                    {
                        if (!string.IsNullOrWhiteSpace(line) && !line.StartsWith("/"))
                        {
                            fileLines.Add(line);
                        }
                    }
                }
            }
            catch
            {
                // cannot do anything here.
            }

            return fileLines;
        }

        /// <summary>
        /// Evaluates a file line and adds its info to the level.
        /// </summary>
        /// <param name="level"></param>
        /// <param name="line"></param>
        private void EvaluateLine(Level level, string line)
        {
            var parts = line.Split(' ', ',').ToList().FindAll(text => !string.IsNullOrWhiteSpace(text));
            if (parts.Count == 0) return;

            switch (parts[0].ToUpper())
            {
                case "START":
                    {
                        level.PlayerPos = new Point3D(int.Parse(parts[1]), int.Parse(parts[2]), int.Parse(parts[3]));
                        break;
                    }
                case "WALL":
                    {
                        Point3D pos = new Point3D(int.Parse(parts[1]), int.Parse(parts[2]), int.Parse(parts[3]));
                        C.Dir dir = ReadDirection(parts[4].ToUpper());
                        bool isRotatable = (parts.Count > 5) && parts[5].ToUpper().StartsWith("R");
                        Wall wall = new Wall(pos, dir, isRotatable);
                        level.Walls.Add(wall);
                        break;
                    }
            }
        }

        /// <summary>
        /// Translates the text direction into a C.Dir.
        /// </summary>
        /// <param name="direction"></param>
        /// <returns></returns>
        private C.Dir ReadDirection(string direction)
        {
            switch (direction[0])
            {
                case 'L': return C.Dir.Left;
                case 'R': return C.Dir.Right;
                case 'U': return C.Dir.Top;
                case 'D': return C.Dir.Bottom;
                case 'F': return C.Dir.Front;
                case 'B': return C.Dir.Back;
                default: return C.Dir.Front;
            }
        }

    }
}
