﻿using System;
using System.Linq;
using System.Windows;

namespace Rubic_Maze
{
    static class Utility
    {
        /// <summary>
        /// Checks if an object is contained in an array.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static bool In<T>(this T obj, params T[] args)
        {
            return args.Contains(obj);
        }

        /// <summary>
        /// Handles a global unexpected exception and displays a warning message to the user.
        /// </summary>
        /// <param name="Ex"></param>
        public static void HandleGlobalException(Exception Ex)
        {
            try
            {
                LogException(Ex);
                string errorString = "An error occured: " + Ex.Message + C.NewLine + "Try to continue playing? Selecting 'no' will quit the game.";
                var result = MessageBox.Show(errorString, "Error", MessageBoxButton.YesNo);
                if (result == MessageBoxResult.No) Application.Current.Shutdown();
            }
            catch
            {
                Application.Current.Shutdown();
            }
        }

        /// <summary>
        /// Logs an exception message to AppPath/ErrorLog.txt.
        /// </summary>
        /// <param name="ex"></param>
        public static void LogException(Exception ex)
        {
            string errorPath = C.AppPath + "ErrorLog.txt";
            using (System.IO.TextWriter textFile = new System.IO.StreamWriter(errorPath, true))
            {
                textFile.WriteLine(ex.ToString());
            }
        }
    }
}
