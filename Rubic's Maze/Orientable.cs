﻿using System;

namespace Rubic_Maze
{
    abstract class Orientable<T> where T : Orientable<T>
    {
        abstract public T TurnRight();
        abstract public T TurnLeft();
        abstract public T FlipBack();

        abstract public T Rotate(C.Dir dir);

        /// <summary>
        /// Computes how the orientable is seen from the player perspective.
        /// </summary>
        /// <param name="absDir"></param>
        /// <param name="playerFeet"></param>
        /// <param name="playerEyes"></param>
        /// <returns></returns>
        public T OrientToPlayer(C.Dir playerFeet, C.Dir playerEyes)
        {
            switch (playerFeet)
            {
                case (C.Dir.Bottom):
                    {
                        switch (playerEyes)
                        {
                            case (C.Dir.Right): return TurnLeft();
                            case (C.Dir.Left): return TurnRight();
                            case (C.Dir.Back): return TurnRight().TurnRight();
                            default: return (T)this;
                        }
                    }
                case (C.Dir.Top):
                    {
                        T feetDir = FlipBack().FlipBack(); // facing now to back
                        switch (playerEyes)
                        {
                            case (C.Dir.Right): return feetDir.TurnLeft();
                            case (C.Dir.Left): return feetDir.TurnRight();
                            case (C.Dir.Front): return feetDir.TurnRight().TurnRight();
                            default: return feetDir;
                        }
                    }
                case (C.Dir.Front):
                    {
                        T feetDir = FlipBack().FlipBack().FlipBack(); // facing now to top
                        switch (playerEyes)
                        {
                            case (C.Dir.Right): return feetDir.TurnLeft();
                            case (C.Dir.Left): return feetDir.TurnRight();
                            case (C.Dir.Bottom): return feetDir.TurnRight().TurnRight();
                            default: return feetDir;
                        }
                    }
                case (C.Dir.Back):
                    {
                        T feetDir = FlipBack(); // facing now to bottom
                        switch (playerEyes)
                        {
                            case (C.Dir.Right): return feetDir.TurnLeft(); 
                            case (C.Dir.Left): return feetDir.TurnRight();
                            case (C.Dir.Top): return feetDir.TurnRight().TurnRight();
                            default: return feetDir;
                        }
                    }
                case (C.Dir.Right):
                    {
                        T feetDir = TurnRight().FlipBack(); 
                        switch (playerEyes)
                        {
                            case (C.Dir.Front): return feetDir.TurnLeft();
                            case (C.Dir.Back): return feetDir.TurnRight(); 
                            case (C.Dir.Top): return feetDir.TurnRight().TurnRight();
                            default: return feetDir;
                        }
                    }
                case (C.Dir.Left):
                    {
                        T feetDir = TurnLeft().FlipBack(); 
                        switch (playerEyes)
                        {
                            case (C.Dir.Front): return feetDir.TurnRight();
                            case (C.Dir.Back): return feetDir.TurnLeft();
                            case (C.Dir.Top): return feetDir.TurnRight().TurnRight();
                            default: return feetDir;
                        }
                    }
                default: return (T)this; // should never happen.
            }
        }

        /// <summary>
        /// Computes the absolute orientation given it in player view.
        /// </summary>
        /// <param name="playerFeet"></param>
        /// <param name="playerEyes"></param>
        /// <returns></returns>
        public T OrientToAbsolute(C.Dir playerFeet, C.Dir playerEyes)
        {
            // Compute how the absolute direction is facing from the player perspective
            var absoluteEyes = new Direction(C.Dir.Front).OrientToPlayer(playerFeet, playerEyes);
            var absoluteFeet = new Direction(C.Dir.Bottom).OrientToPlayer(playerFeet, playerEyes);
            // Rotate now using the player as absolute reference.
            return OrientToPlayer(absoluteFeet.Dir, absoluteEyes.Dir);
        }

        abstract public T RotateFromPlayer(C.Dir playerFeet, C.Dir playerEyes, Point3D playerPos, C.Dir rotateDir);

    }
}
