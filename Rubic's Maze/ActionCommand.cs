﻿using System;
using System.Windows.Input;

namespace Rubic_Maze
{
    /// <summary>
    /// An ICommand implementation that always calls an Action without parameters.
    /// </summary>
    class ActionCommand : ICommand
    {
        public ActionCommand(Action execute)
        {
            this.execute = execute;
        }

        private readonly Action execute;

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            execute();
        }

        public event EventHandler CanExecuteChanged;
    }

    /// <summary>
    /// An ICommand implementation that always calls an Action with a single parameter.
    /// </summary>
    class DelegateCommand : ICommand
    {
        public DelegateCommand(Action<object> execute)
        {
            this.execute = execute;
        }

        private Action<object> execute;

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            execute(parameter);
        }

        public event EventHandler CanExecuteChanged;
    }


}
