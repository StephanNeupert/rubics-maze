﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Media;

namespace Rubic_Maze
{
    class Renderer
    {
        public Renderer(Level level)
        {
            this.level = level;
        }

        Level level;

        /// <summary>
        /// Creates a new observable collection with all the wall shapes.
        /// </summary>
        /// <returns></returns>
        public ObservableCollection<WallShape> CreateImage()
        {
            var wallShapes = new ObservableCollection<WallShape>();

            foreach (Wall wall in GetSortedWallList())
            {
                wallShapes.Add(GetWallShape(wall));
            }

            return wallShapes;
        }

        /// <summary>
        /// Gets a sorted list of all walls visible from the player perspective.
        /// </summary>
        /// <returns></returns>
        private List<Wall> GetSortedWallList()
        {
            var walls = level.WallsFromPlayer.FindAll(wall => wall.IsVisible());
            walls.Sort();
            return walls;
        }

        /// <summary>
        /// Creates the geometry object displaying the wall.
        /// </summary>
        /// <param name="wall"></param>
        /// <returns></returns>
        private WallShape GetWallShape(Wall wall)
        {
            var shapeFactory = new WallShapeFactory();

            // Global drawing properties
            shapeFactory.Stroke = Brushes.Black;
            shapeFactory.StrokeThickness = 1;
            shapeFactory.Fill = new SolidColorBrush(GetWallColor(wall));
            // Set points
            var points = WallVertices(wall);
            shapeFactory.Points = points;
            // Position the wall polygon on the canvas
            shapeFactory.CanvasLeft = 0;
            shapeFactory.CanvasTop = 0; 

            return shapeFactory.Create();
        }

        /// <summary>
        /// Creates a collection of all vertices of the wall, as displayed on the screen.
        /// </summary>
        /// <param name="wall"></param>
        /// <returns></returns>
        private PointCollection WallVertices(Wall wall)
        {
            var points = new PointCollection(4);
            switch (wall.Dir.Dir)
            {
                case C.Dir.Front:
                    {
                        points.Add(ProjectPoint(new Point3D(2 * wall.Pos.X - 1, 2 * wall.Pos.Y + 1, wall.Pos.Z + 1)));
                        points.Add(ProjectPoint(new Point3D(2 * wall.Pos.X + 1, 2 * wall.Pos.Y + 1, wall.Pos.Z + 1)));
                        points.Add(ProjectPoint(new Point3D(2 * wall.Pos.X + 1, 2 * wall.Pos.Y - 1, wall.Pos.Z + 1)));
                        points.Add(ProjectPoint(new Point3D(2 * wall.Pos.X - 1, 2 * wall.Pos.Y - 1, wall.Pos.Z + 1)));
                        break;
                    }
                case C.Dir.Back:
                    {
                        points.Add(ProjectPoint(new Point3D(2 * wall.Pos.X - 1, 2 * wall.Pos.Y + 1, wall.Pos.Z)));
                        points.Add(ProjectPoint(new Point3D(2 * wall.Pos.X + 1, 2 * wall.Pos.Y + 1, wall.Pos.Z)));
                        points.Add(ProjectPoint(new Point3D(2 * wall.Pos.X + 1, 2 * wall.Pos.Y - 1, wall.Pos.Z)));
                        points.Add(ProjectPoint(new Point3D(2 * wall.Pos.X - 1, 2 * wall.Pos.Y - 1, wall.Pos.Z)));
                        break;
                    }
                case C.Dir.Top:
                    {
                        points.Add(ProjectPoint(new Point3D(2 * wall.Pos.X - 1, 2 * wall.Pos.Y + 1, wall.Pos.Z)));
                        points.Add(ProjectPoint(new Point3D(2 * wall.Pos.X + 1, 2 * wall.Pos.Y + 1, wall.Pos.Z)));
                        points.Add(ProjectPoint(new Point3D(2 * wall.Pos.X + 1, 2 * wall.Pos.Y + 1, wall.Pos.Z + 1)));
                        points.Add(ProjectPoint(new Point3D(2 * wall.Pos.X - 1, 2 * wall.Pos.Y + 1, wall.Pos.Z + 1)));
                        break;
                    }
                case C.Dir.Bottom:
                    {
                        points.Add(ProjectPoint(new Point3D(2 * wall.Pos.X - 1, 2 * wall.Pos.Y - 1, wall.Pos.Z)));
                        points.Add(ProjectPoint(new Point3D(2 * wall.Pos.X + 1, 2 * wall.Pos.Y - 1, wall.Pos.Z)));
                        points.Add(ProjectPoint(new Point3D(2 * wall.Pos.X + 1, 2 * wall.Pos.Y - 1, wall.Pos.Z + 1)));
                        points.Add(ProjectPoint(new Point3D(2 * wall.Pos.X - 1, 2 * wall.Pos.Y - 1, wall.Pos.Z + 1)));
                        break;
                    }
                case C.Dir.Right:
                    {
                        points.Add(ProjectPoint(new Point3D(2 * wall.Pos.X + 1, 2 * wall.Pos.Y + 1, wall.Pos.Z)));
                        points.Add(ProjectPoint(new Point3D(2 * wall.Pos.X + 1, 2 * wall.Pos.Y - 1, wall.Pos.Z)));
                        points.Add(ProjectPoint(new Point3D(2 * wall.Pos.X + 1, 2 * wall.Pos.Y - 1, wall.Pos.Z + 1)));
                        points.Add(ProjectPoint(new Point3D(2 * wall.Pos.X + 1, 2 * wall.Pos.Y + 1, wall.Pos.Z + 1)));
                        break;
                    }
                case C.Dir.Left:
                    {
                        points.Add(ProjectPoint(new Point3D(2 * wall.Pos.X - 1, 2 * wall.Pos.Y + 1, wall.Pos.Z)));
                        points.Add(ProjectPoint(new Point3D(2 * wall.Pos.X - 1, 2 * wall.Pos.Y - 1, wall.Pos.Z)));
                        points.Add(ProjectPoint(new Point3D(2 * wall.Pos.X - 1, 2 * wall.Pos.Y - 1, wall.Pos.Z + 1)));
                        points.Add(ProjectPoint(new Point3D(2 * wall.Pos.X - 1, 2 * wall.Pos.Y + 1, wall.Pos.Z + 1)));
                        break;
                    }
            }
            return points;
        }

        /// <summary>
        /// Projects a given 3D point to the drawing plane.
        /// <para> Vertices of walls are at odd X, Y coordinates, but all Z corrdinates.</para>
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        private Point ProjectPoint(Point3D point)
        {
            double squareSize = (point.Z <= 0) ? C.CanvasWidth * 2 : C.CanvasWidth * 0.7 * Math.Pow(C.ShrinkFactor, point.Z); 
            double posX = C.CanvasWidth * 0.5 + squareSize * point.X / 2;
            double posY = C.CanvasHeight * 0.5 - squareSize * (point.Y - 0.2) / 2;

            return new Point(posX, posY);
        }

        /// <summary>
        /// Determine the color for the wall.
        /// </summary>
        /// <param name="wall"></param>
        /// <returns></returns>
        private Color GetWallColor(Wall wall)
        {
            byte gray = (byte)(255 - (wall.Pos.Z + 1) * 60);

            // Adapt to direction
            switch (wall.Dir.Dir)
            {
                case C.Dir.Front: gray -= 30; break;
                case C.Dir.Top: gray -= 10; break;
                case C.Dir.Bottom: gray += 10; break;
                case C.Dir.Back: gray += 30; break;
            }

            byte red = gray;
            byte green = gray;
            byte blue = gray;

            // Apapt to recoloring (for rotatable and back walls) 
            if (wall.IsSeenFromBack())
            {
                red = (byte)Math.Max(red - 60, 0);
                green = (byte)Math.Max(green - 20, 0);
                blue = (byte)Math.Min(blue + 60, 255);
            }
            else if (wall.IsRotatable)
            {
                red = (byte)Math.Min(red + 60, 255); 
                green = (byte)Math.Max(green - 50, 0);
                blue = (byte)Math.Max(blue - 50, 0);
            }

            return Color.FromArgb(255, red, green, blue);
        }
    }
}
