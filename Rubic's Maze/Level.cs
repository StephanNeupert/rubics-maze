﻿using System;
using System.Collections.Generic;

namespace Rubic_Maze
{
    /// <summary>
    /// Contains all information about the current state of a level.
    /// </summary>
    class Level
    {
        public Level()
        {
            PlayerPos = new Point3D(1, 1, 0);
            PlayerFeet = C.Dir.Bottom;
            PlayerEyes = C.Dir.Front;

            AddBorderWalls();
        }

        public Level(string filePath)
        {
            Walls = new List<Wall>();
            PlayerPos = new Point3D(1, 1, 0);
            PlayerFeet = C.Dir.Bottom;
            PlayerEyes = C.Dir.Front;

            var loader = new LevelLoader();
            loader.Load(filePath, this);
            AddBorderWalls();
        }

        public Point3D PlayerPos { get; set; }
        public C.Dir PlayerFeet { get; private set; }
        public C.Dir PlayerEyes { get; private set; }

        public List<Wall> Walls { get; private set; }
        public List<Wall> WallsFromPlayer => GetWallsFromPlayer();

        public Level Clone()
        {
            Level newLevel = new Level();
            newLevel.PlayerPos = new Point3D(PlayerPos);
            newLevel.PlayerFeet = PlayerFeet;
            newLevel.PlayerEyes = PlayerEyes;
            newLevel.Walls = Walls.ConvertAll(wall => wall.Clone());
            return newLevel;
        }

        /// <summary>
        /// Checks whether the level is solved, i.e. we reached the middle.
        /// </summary>
        /// <returns></returns>
        public bool IsSolved()
        {
            return PlayerPos.Equals(new Point3D(C.MidCoord, C.MidCoord, C.MidCoord));
        }

        /// <summary>
        /// Adds borders wall, where none are yet.
        /// </summary>
        private void AddBorderWalls()
        {
            if (Walls == null) Walls = new List<Wall>();

            // Front and back
            for (int x = 0; x < C.Size; x++)
            {
                for (int y = 0; y < C.Size; y++)
                {
                    AddWall(x, y, C.Size - 1, C.Dir.Front, false);
                    AddWall(x, y, 0, C.Dir.Back, false);
                }
            }
            // Top and bottom
            for (int x = 0; x < C.Size; x++)
            {
                for (int z = 0; z < C.Size; z++)
                {
                    AddWall(x, C.Size - 1, z, C.Dir.Top, false);
                    AddWall(x, 0, z, C.Dir.Bottom, false);
                }
            }
            // Sides
            for (int y = 0; y < C.Size; y++)
            {
                for (int z = 0; z < C.Size; z++)
                {
                    AddWall(C.Size - 1, y, z, C.Dir.Right, false);
                    AddWall(0, y, z, C.Dir.Left, false);
                }
            }
        }

        /// <summary>
        /// Adds a wall at a given location, if the level does not yet have a wall there.
        /// </summary>
        /// <param name="posX"></param>
        /// <param name="posY"></param>
        /// <param name="posZ"></param>
        /// <param name="dir"></param>
        /// <param name="isRotatable"></param>
        private void AddWall(int posX, int posY, int posZ, C.Dir dir, bool isRotatable)
        {
            Wall newWall = new Wall(new Point3D(posX, posY, posZ), dir, isRotatable);
            if (!Walls.Exists(wall => wall.Equals(newWall)))
            {
                Walls.Add(newWall);
            }
        }

        /// <summary>
        /// Gets a list of walls as seen from the player. 
        /// </summary>
        /// <returns></returns>
        private List<Wall> GetWallsFromPlayer()
        {
            return Walls.ConvertAll(wall => GetWallFromPlayer(wall));
        }

        /// <summary>
        /// Returns a wall as seen from the player.
        /// </summary>
        /// <param name="absWall"></param>
        /// <returns></returns>
        private Wall GetWallFromPlayer(Wall absWall)
        {
            // Move player coordinate to zero
            Point3D newPos = new Point3D(absWall.Pos.X - PlayerPos.X,
                                         absWall.Pos.Y - PlayerPos.Y,
                                         absWall.Pos.Z - PlayerPos.Z);
            newPos = newPos.OrientToPlayer(PlayerFeet, PlayerEyes);

            Direction newDir = absWall.Dir.OrientToPlayer(PlayerFeet, PlayerEyes);
            return new Wall(newPos, newDir, absWall.IsRotatable);
        }

        /// <summary>
        /// Tries to rotate the face of the cube with the wall the player is facing.
        /// </summary>
        /// <param name="dir"></param>
        /// <returns></returns>
        public bool TryRotateWall(C.Dir dir)
        {
            System.Diagnostics.Debug.Assert(dir.In(C.Dir.Left, C.Dir.Right), "Rotating a wall tried for a direction not left or right.");

            if (!Walls.Exists(wall => wall.IsRotatable && wall.Pos.Equals(PlayerPos) && wall.Dir.Dir.Equals(PlayerEyes))) return false;

            foreach (Wall wall in GetRotatableWalls())
            {
                wall.Pos = wall.Pos.RotateFromPlayer(PlayerFeet, PlayerEyes, PlayerPos, dir);
                wall.Dir = wall.Dir.RotateFromPlayer(PlayerFeet, PlayerEyes, PlayerPos, dir);
            }      

            return true;
        }

        /// <summary>
        /// Gets all walls in the player face.
        /// </summary>
        /// <returns></returns>
        private List<Wall> GetRotatableWalls()
        {
            switch (PlayerEyes)
            {
                case C.Dir.Front:
                case C.Dir.Back:
                    {
                        return Walls.FindAll(wall => wall.Pos.Z == PlayerPos.Z);
                    }
                case C.Dir.Right:
                case C.Dir.Left:
                    {
                        return Walls.FindAll(wall => wall.Pos.X == PlayerPos.X);
                    }
                case C.Dir.Top:
                case C.Dir.Bottom:
                    {
                        return Walls.FindAll(wall => wall.Pos.Y == PlayerPos.Y);
                    }
                default: return Walls; // should never happen
            }
        }

        /// <summary>
        /// Tries to move the player in a given direction.
        /// </summary>
        /// <param name="playerMoveDir"></param>
        /// <returns></returns>
        public bool TryMovePlayer(C.Dir playerMoveDir)
        {
            var absMoveDir = new Direction(playerMoveDir).OrientToAbsolute(PlayerFeet, PlayerEyes);
            Point3D targetPos = PlayerPos.Move(absMoveDir.Dir);

            if (   Walls.Exists(wall => wall.Pos.Equals(PlayerPos) && wall.Dir.Equals(absMoveDir))
                || Walls.Exists(wall => wall.Pos.Equals(targetPos) && wall.Dir.Equals(absMoveDir.Opposite())))
            {
                return false;
            }
            else
            {
                PlayerPos = targetPos;
                return true;
            }
        }

        /// <summary>
        /// Rotates the player view.
        /// </summary>
        /// <param name="direction"></param>
        public void RotatePlayer(C.Dir direction)
        {
            /* C.Dir.Left:   Rotate around y-axis to face left
             * C.Dir.Right:  Rotate around y-axis to face right
             * C.Dir.Top:    Rotate around x-axis to face up
             * C.Dir.Bottom: Rotate around x-axis to face down
             * C.Dir.Front:  Rotate around z-axis to feet to left
             * C.Dir.Back:   Rotate around z-axis to feet to right */
            switch (direction)
            {
                case C.Dir.Left:
                    {
                        PlayerEyes = (new Direction(C.Dir.Left)).OrientToAbsolute(PlayerFeet, PlayerEyes).Dir;
                        break;
                    }
                case C.Dir.Right:
                    {
                        PlayerEyes = (new Direction(C.Dir.Right)).OrientToAbsolute(PlayerFeet, PlayerEyes).Dir;
                        break;
                    }
                case C.Dir.Top:
                    {
                        C.Dir oldEyes = PlayerEyes;
                        PlayerEyes = (new Direction(C.Dir.Top)).OrientToAbsolute(PlayerFeet, PlayerEyes).Dir;
                        PlayerFeet = oldEyes;
                        break;
                    }
                case C.Dir.Bottom:
                    {
                        C.Dir oldEyes = PlayerEyes;
                        PlayerEyes = (new Direction(C.Dir.Bottom)).OrientToAbsolute(PlayerFeet, oldEyes).Dir;
                        PlayerFeet = (new Direction(C.Dir.Back)).OrientToAbsolute(PlayerFeet, oldEyes).Dir;
                        break;
                    }
                case C.Dir.Front:
                    {
                        PlayerFeet = (new Direction(C.Dir.Left)).OrientToAbsolute(PlayerFeet, PlayerEyes).Dir;
                        break;
                    }
                case C.Dir.Back:
                    {
                        PlayerFeet = (new Direction(C.Dir.Right)).OrientToAbsolute(PlayerFeet, PlayerEyes).Dir;
                        break;
                    }
            }

        }

    }
}
